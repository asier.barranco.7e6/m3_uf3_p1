# M3_UF3_P1

- [Introducció](#introduccio)
- [Objectius](#objectius)
- [Funcions](#funcions)
- [Documentació](#documentacio)
- [Conclusions i millores](#conclusions)

## Introducció <a name="introduccio"></a>
El gimnás <span style="Color: blue">ASMA</span> ens contracta per implementar un sistema d’alta i gestió d’usuaris. Ells ens passarán les dades i les haurem de manipular (perquè el seu sistema està obsolet) per manegar-les i treballar millor amb elles. Aquest nou sistema permetrà donar d’alta a nous usuaris així com enregistrar canvis sobre aquests.


## Objectius <a name="objectius"></a>
### - Importar / Transformar les dades que ens donen: 
Ens passarán un fitxer de text amb el següent format: (Usuari 1)ID,NomICognom,Telefon,Email,Actiu,Bloquejat;(Usuari2)ID,NomICognom,Telefon,Email,Actiu,Bloquejat;
És a dir, les dades del mateix usuari separades per comes, i els usuaris separats entre sí amb punt i coma.
Nosaltres haurem d'implementar-los en un altre fitxer de text i fer una línia per a cada usuari, separant cada dada de l'usuari amb punt i coma i assignant-li una nova ID a cadascún. Els usuaris que estiguin bloquejats o inactius no cal importar-los

### - Crear / Editar usuaris: 
Haurem de permetre la creació de usuaris així com la modificació dels mateixos, incolent una opció de bloquejar o desbloquejar usuaris.

### - Bakcups: 
Ens han demanat també crear una opció per guardar dades i fer còpies de seguretat. Només es pot fer una per día. Al sortir es guardarà automàticament.

## Funcions<a name="funcions"></a>

##### Funcions implementades:

    -   mostrarMenu -> Mostra el menú d'opcions
    -   convertData -> Genera una línia per a cadascun dels usuaris importats
    -   importardades -> Importa les dades en el format que es desitja en un nou fitxer
    -   assignarID -> Revisa l'últim ID del document i ens genera un més per poder donar-li a nous usuaris
    -   dataExists -> Comprova si s'han importat dades
    -   createUser -> Crea un usuari i l'afegeix al final del document on es guarden tots
    -   editUser -> Edita els valors de l'usuari desitjat, si existeix
    -   blockOrUnblockUser -> Edita el valor del camp que indica si l'usuari està bloquejat
    -   userExists -> Comproba si existeix l'usuari
    -   dailyBackup -> Fa una còpia de seguretat de les dades enregistrades amb la data del día actual. Límit 1


## Conclusions i millores<a name="conclusions"></a>
Penso que el projecte ens ha servit per tenir una primera toma de contacte per treballar en un mateix projecte amb diferents fitxers alhora. Et fa adornar-te que ets tú qui t'has adaptar les empreses i a la seva metodologia perquè no t'enviarán tot en el format que a tú t'agrada o necessites. Aquesta pràctica ens un cop de realitat.

Hi ha moltes coses a millorar i/o arreglar, perquè no hem pogut implementar tot a la perfecció:

- No es genera una ID nova per als usuaris que ja existíen (Sí per als nous)
- Les dades només es poden importar del document "import.txt"
- La funció de bloquejar/desbloquejar no acaba de funcionar del tot


###### Asier Barranco & Arnau Pajares
