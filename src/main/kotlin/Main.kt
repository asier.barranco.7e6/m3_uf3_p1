import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.io.path.*
fun main() {

    //Primera importació de dades
    importardades()

    do{
        //Es mostra el menú
        mostrarMenu()
        print("Tria una opció: "); val opc = readln().toInt() //L'usuari tria una opció del menú
        when (opc){
            1 -> {
                //L'usuari tria l'1
                val import = Path("./import/")
                if (import.listDirectoryEntries().isEmpty()){ //Primer es comprova si hi ha dades per importar
                    println("No hi ha dades per importar")

                } else {
                    importardades() //Si hi ha dades, s'importen les dades i s'informa
                    println("Dades importades amb èxit")
                }

            }
            2->{
                //L'usuari tria el 2

                createUser()

            }
            3->{
                //L'usuari tria el 3
                editUser()
            }
            4->{
                //L'usuari tria el 4
                blockOrUnblockUser()
            }
            5->{
                //L'usuari tria el 5
                dailyBackup() //Es fa una còpia de seguretat
            }
            0-> {
                //L'usuari tria el 0. Sortir
                println("Fins aviat")
                dailyBackup() //Es fa un autoguardat de seguretat
            }
            else -> println("ERROR") //Si tria una opció que no està al menú es mostra un missatge d'error
        }
    } while (opc!=0) //El bucle es repeteix fins que es tria el 0, que és l'opció de sortida

}

/**
 * Funció que mostra el menú d'opcions
 * @author Asier Barranco
 * @version 31/01/2023
 */
fun mostrarMenu() {
    println("""
                |###########################
                |# MENÚ                    #
                |# 1. Importar dades       #
                |# 2. Crear usuari         #
                |# 3. Editar usuari        #
                |# 4. Des/bloquejar usuari #
                |# 5. Còpia de seguretat   #
                |# 0. Sortida              # 
                |###########################
    """.trimIndent())
}

/**
 * Funció que genera una línia per a cadascun dels usuaris importats
 * @author Arnau Pajares
 * @version 01/02/2023
 * @parameter Path del fitxer on están les dades per refactoritzar
 * @return Contingut de les dades refactoritzades
 */

fun convertData(ruta: String):List<String>{

    val originalFile = File(ruta) //Document del que es volen refactoritzar les dades
    val str = originalFile.readText() //Llegim les dades del fitxer
    val content: List<String> = str.split(";") //Es divideix per cada ';' que hi hagi
    return content
}
/**
 * Funció que afegeix dades a un nou fitxer
 * @author Arnau Pajares & Asier Barranco
 * @version 01/02/2023
 */
fun importardades(){

    val targetfile = File("./data/userData.txt") //Fitxer on es volen implementar les dades
    val dades=convertData("./import/import.txt") //Cridada a la funció que transforma el contingut
    for(item in dades) {
        targetfile.appendText(item.replace(',', ';') + "\n") //Replacem les , que separen les dades de cada usuari per ;
    }
}


/**
 * Funció que comprova quin és l'últim ID que hi ha al document i retorna 1 més
 * @author Arnau Pajares & Asier Barranco
 * @version 06/02/2023
 * @return Número enter que fa referéncia a un ID assignable
 */
fun assignarID(): String{

    val userData = File("./data/userData.txt")
    if (dataExists("./data/userData.txt")) return "1" //Si no hi ha dades al document, l'ID a assignar és 1.
    val lastUser = (userData.readLines()[userData.readLines().size-1]) //Accedim a l'últim usuari enregistrat al document
    val id = lastUser.split(";").toMutableList()[0] //Mirem la posició inicial (és a dir, l'ID) de l'últim enregistrament.
    return id + 1
//Retornem aquesta ID + 1 per poder-la assignar
}
/**
 * Funció que comprova si hi ha dades en el document
 * @author Arnau Pajares & Asier Barranco
 * @version 06/02/2023
 * @parameter Ruta del fitxer a comprovar
 * @return Booleà que determina si hi ha usuaris importats en el document
 */
 fun dataExists(path:String):Boolean{
     val file = File(path) //Ruta del fitxer
     return (file.length() == 0L) //Retorna true o false depenent de si existeix llargada al document
 }

/**
 * Funció que demana dades a l'usuari per crear un nou usuari i el fica al final del fitxer desitjat
 * @author Asier Barranco
 * @version 07/02/2023
 */
fun createUser(){

    val originalFile = File("./data/userData.txt")
    var user = mutableListOf<String>() //Les dades es guarden en una llista
    println("Introdueix les dades del nou usuari: ") //Demanem dades i les anem afegint a la llista
    print("Nom: "); val nom = readLine()!!; user.add(nom)
    print("Telèfon: "); val tlf = readLine()!!.toString(); user.add(tlf)
    print("Correu: "); val email = readLine()!!; user.add(email)
    val actiu = "true"; user.add(actiu)
    val blocked = "false"; user.add(blocked)
    originalFile.appendText(assignarID().toString())
    originalFile.appendText("${user.toString().replace("[", "").replace("]", "")}\n") //Afegim el nou usuari al document

}

/**
 * Funció que demana un id i, si existeix, torna a introduïr les dades d'aquest per modificar-lo
 * @author Asier Barranco
 * @version 07/02/2023
 */
fun editUser(){

    val originalFile = File("./data/userData.txt")
    println("Introdueix el ID de l'usuari: "); val numID = readln().toInt()
    if (userExists(numID.toString())) {


        var editedUser = mutableListOf<String>() //Les dades es guarden en una llista
        println("Edita les dades del usuari: ")
        print("Nom: ");
        val nom = readLine()!!; editedUser.add(nom)
        print("Telèfon: ");
        val tlf = readln().toString(); editedUser.add(tlf)
        print("Correu: ");
        val email = readLine()!!; editedUser.add(email)
        val actiu = "true"; editedUser.add(actiu)
        val blocked = "false"; editedUser.add(blocked)
        originalFile.appendText(numID.toString())
        editedUser.toString().replace("[", "").replace("]", "") //Canviem les [ que per defecte crea la llista
        originalFile.appendText("${editedUser.toString().replace("[", "").replace("]", "")}\n") //Afegim el nou usuari al document

    } else println("L'usuari no existeix")


}

/**
 * Funció que canvia l'estat de bloquejat
 * @author Asier Barranco
 * @version 07/02/2023
 */
fun blockOrUnblockUser(){

    val originalFile = File("./data/userData.txt")
    println("Introdueix el ID de l'usuari: "); val numID = readln().toInt()
    val dades = mutableListOf<String>()
    originalFile.forEachLine { dades.add(it) }
    if (userExists(numID.toString())) {


        println("Vols bloquejar (1) o desbloquejar (2) ?: "); val opc = readln().toInt()
        if (opc == 1) {
            for (index in dades){
                if (dades[numID] == dades[index.toInt()]) dades[4]="true"
            }


        } else if (opc == 2) {
            for (index in dades){
                if (dades[numID] == dades[index.toInt()]) dades[4]="false"
            }

        } else println("ERROR")
    } else println("L'usuari no existeix")


}

/**
 * Funció que comprova si existeis un usuari amb aquesta ID
 * @parameter ID de l'usuari que es vol comprovar
 * @return Booleà depenent si troba un usuari amb el mateix ID
 * @version 07/02/2023
 * @author Asier Barranco
 */
fun userExists(ID : String): Boolean{
    val originalFile = File ("./data/userData.txt")
    val dades = mutableListOf<String>()
    originalFile.forEachLine { dades.add(it) }

    for (index in dades.indices){
        val idExistent = dades[index].split(";")[0].toString()
        if (idExistent.toString() == ID.toString()) return true
    }
    return false
}
/**
 * Funció que crea o modifica un document de còpia de seguretat
 * @author Asier Barranco
 * @version 01/02/2023
 */
fun dailyBackup(){

    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val current = LocalDate.now().format(formatter) //Data d'avui amb el format desitjat
    val originalFile = File("./data/userData.txt") //Fitxer d'on provenen les dades
    val dailyBackupFile = Path("./backups/${current}userData.txt") //Fitxer objectiu on anirán aquestes dades

    if (dailyBackupFile.exists()){ //Si ja s'ha fet el document el mateix dia, s'actualitza
        File("./data/userData.txt").copyTo(File("./backups/${current}userData.txt"), true)
        println("Còpia de seguretat actualitzada amb èxit")
    } else { //Si aquest dia no hi ha còpia guardada, es crea una
        File("./data/userData.txt").copyTo(File("./backups/${current}userData.txt"))
        println("Còpia de seguretat enregistrada amb èxit")
    }

}
